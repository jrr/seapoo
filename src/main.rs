#![allow(dead_code)]

extern crate hex;
extern crate indicatif;

use indicatif::ProgressBar;
use std::collections::HashMap;

struct CPU {
    cycles: usize,
    labels: HashMap<u8, usize>,
    memory: [u8; 65535],
    program_counter: usize,
}

const MAX_CYCLES: u64 = 1024 * 100;

const OPCODES: [&str; 5] = [
    "add",
    "dmp",
    "jmp",
    "lbl",
    "ldr",
];

const SECRET_START: usize = 0xc350;

const REG_ACC: usize = 0x00 << 8;

fn as_char(string: &str) -> char {
    match String::from(string).pop() {
        Some('0') => '.',
        Some(s) => s,
        None => ' ',
    }
}

fn splut(string: String) -> String {
    let mut chars = string.split("");
    let mut out_vec: Vec<u8> = vec![];

    // throw away string start
    chars.next();
    for i in 1..string.len() {
        match chars.next() {
            Some(c) => out_vec.push(as_char(c) as u8),
            None => { break },
        }
        if i % 2 == 0 {
            out_vec.push(' ' as u8);
        }
        if i % 16 == 0 {
            out_vec.push('\n' as u8);
        }
    }
    match String::from_utf8(out_vec) {
        Ok(strung) => strung,
        Err(err) => {
            println!("{}", err);
            return String::from("")
        },
    }
}

fn new_cpu() -> CPU {
    CPU {
        cycles: 0,
        labels: HashMap::new(),
        memory: [0; 65535],
        program_counter: 0,
    }
}

impl CPU {
    pub fn dump_from(&self, start: usize) {
        let data = &self.memory[start..];
        for (k, v) in &self.labels {
            println!("Label {:x} @ {:x}", k, v);
        }
        println!("{}", hex::encode(data));
    }

    pub fn dump(&self, start: usize, end: usize) {
        if end == 0 {
            self.dump_from(start);
            return;
        }

        let data = &self.memory[start..end];
        println!("{}", splut(hex::encode(data)));
    }

    fn cur_byte(&mut self) -> u8 {
        let b = self.memory[self.program_counter];
        self.program_counter += 1;
        return b
    }

    pub fn exec_at(&mut self, location: usize) {
        self.program_counter = location;
        self.run();
    }

    pub fn load_program(&mut self, start: usize, data: Vec<u8>) {
        let mut position = start;
        for byte in data {
            self.memory[position] = byte;
            position += 1;
        }
    }

    pub fn run(&mut self) {
        let pb = ProgressBar::new(MAX_CYCLES);
        pb.set_draw_delta(100);
        loop {
            if self.cycles > (MAX_CYCLES as usize) {
                pb.finish_with_message("Maximum cycles exceeded");
                return
            }
            match self.cur_byte() {
                0x03 => self.ldr(),
                0x05 => {
                    //println!("adding label {:x} at {:x}", self.peek(), self.program_counter);
                    self.lbl();
                },
                0xff => {
                    //println!("adding {} to accumulator", self.peek());
                    self.add();
                },
                //0xfe => self.dmp(),
                0xaa => self.jmp(),
                _ => {},
            }
            self.cycles += 1;
            pb.inc(1);
        }
    }

    fn add(&mut self) {
        let val = self.cur_byte();
        let reg = REG_ACC;
        match self.memory[reg].checked_add(val) {
            None => self.memory[reg] = 0,
            Some(total) => self.memory[reg] = total,
        }
    }

    fn sub(&mut self) {
        let val = self.cur_byte();
        let reg = REG_ACC;
        match self.memory[reg].checked_sub(val) {
            None => self.memory[reg] = 0,
            Some(total) => self.memory[reg] = total,
        }
    }

    fn dmp(&mut self) {
        let reg = self.cur_byte();
        let registers: HashMap<u8, &str> = [
            (0x00, "acc"),
            (0x01, "dr1"),
            (0x02, "dr2"),
            (0x03, "dr3")
        ].iter().cloned().collect();
        println!("DMP {}: {:x}", registers[&reg], self.memory[reg as usize]);
    }

    fn jmp(&mut self) {
        let dst = self.cur_byte() as usize;
        self.program_counter = dst;
    }

    fn lbl(&mut self) {
        let label = self.cur_byte();
        self.labels.insert(label, self.program_counter);
    }

    fn ldr(&mut self) {
        let (reg, data) = (self.cur_byte(), self.cur_byte());
        self.memory[reg as usize] = data;
    }

    fn mov(&mut self) {
        let (src, dst) = (self.cur_byte(), self.cur_byte());
        self.memory[dst as usize] = self.memory[src as usize];
        self.memory[src as usize] = 0;
    }

    fn peek(&self) -> u8 {
        self.memory[self.program_counter]
    }

    fn user_mem(loc: u8) -> u16 {
        (loc as u16) << 8
    }
}

fn main() {
    let vector: Vec<u8> = vec!(0xff, 0xaa, 0x03, 0x01, 0xf0, 0xfe, 0x01, 0xfe, 0x00, 0x05, 0xa, 0xaa, 0xc3);

    let mut cpu = new_cpu();
    cpu.load_program(0x0100, vector);
    cpu.exec_at(0x0100);
    println!("\nRegisters\n");
    cpu.dump(0x00, 0xff);
    println!("\nProgram\n");
    cpu.dump(0x0100, 0x01ff);
}
